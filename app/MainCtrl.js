angular.module('app')
	.controller('MainCtrl', MainCtrl);

MainCtrl.$inject = ['$scope'];

function MainCtrl($scope) {
	var vm = this;
	
    var validationTimeout = null;

	vm.initValue = '';

	vm.aceLoaded = function(editor) {
        //disable "Shift-Enter" key combination
        editor.commands.bindKey("Shift-Enter", "null");

        //prevent code pasting with newlines
        editor.on("paste", function(e) {
            e.text = e.text.replace(/[\r\n]+/g, " ");
        });

        //listen to keypress events to prevent new lines
        editor.container.addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode;
            if (key !== 13) { // 13 is enter
                return;
            }
            e.preventDefault();
            //TODO execute action!
        });

        //validation
        editor.container.addEventListener('keyup', function(e) {
            if(validationTimeout != null) {
                clearTimeout(validationTimeout);
                validationTimeout = null;
            }

            validationTimeout = setTimeout(function() {
                var isValid = validate(editor.getSession().getValue());
                if(isValid) {
                    $("#wrapper").removeClass("invalid");
                } else {
                    $("#wrapper").addClass("invalid");
                }
            }, 450);
        });
    }

    function validate(input) {
        var parse = module.exports.parse;
        try {
            var q = parse(input)
            if(q) {
                return true;
            }
        } catch(err) {
            return false;
        }     
    }

    vm.exeGo = function() {
        console.log("isValid: " + $scope.myForm.$valid);
        if(!$scope.myForm.$valid) {
            //TODO:     
        }
    }
}	